let requestURL = "/numberOfMatchesWonPerTeamPerYear.json";
let request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function () {
  let jsonObject = request.response;
  let jsonArray = [];
  for (let teamObject in jsonObject) {
    jsonArray.push([teamObject, jsonObject[teamObject]]);
  }
  
  let allYears = [];
  allYears = jsonArray.reduce((allYears, element) => {
    for (let year in element[1]) {
      if (allYears.includes(year) === false) {
        allYears.push(year);
      }
    }
    return allYears;
  }, []);
  allYears.sort((a, b) => Number(a) - Number(b));

  let seriesData = [];
  seriesData = allYears.reduce((seriesData, year) => {
    seriesData.push({
      name: year,
      data: Array(jsonArray.length).fill(null)
    });
    return seriesData;
  }, []);

  for (let i = 0; i <= (seriesData.length - 1); i++) {
    let data = seriesData[i].data;
    let year = seriesData[i].name;
    for (let j = 0; j <= (data.length - 1); j++) {
      if(jsonArray[j][1].hasOwnProperty(year)) {
        data[j] = jsonArray[j][1][year];
      }
    }
  }
  
  let xValues = jsonArray.map(element => element[0]);
  let options = {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Number of matches won per team per year in IPL.'
    },
    xAxis: {
      title: {
          text: 'Team',
      },
      categories: xValues,
    },
    yAxis: {
      title: {
        text: 'Number of matches won'
      }
    },
    tooltip: {
      shared: true,
    },
    series: seriesData
  };
  Highcharts.chart('container', options);
};
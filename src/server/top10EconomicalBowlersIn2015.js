let requestURL = "/top10EconomicalBowlersIn2015.json";
let request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function () {
  let jsonArray = request.response;
  let xValues = [];
  let yValues = [];
  for (let bowlerObject of jsonArray) {
    xValues.push(bowlerObject["Name"]);
    yValues.push(bowlerObject["Economy Rate"]);
  }
  let options = {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Top 10 economical bowlers in 2015'
    },
    xAxis: {
      title: {
          text: 'Bowler',
      },
      categories: xValues,
    },
    yAxis: {
      title: {
        text: 'Economy Rate'
      }
    },
    series: [
      {
        name: "Economy Rate",
        showInLegend: false,
        data: yValues
      }
    ]
  };
  Highcharts.chart('container', options);
};
// solving stat 1
var csvToJson = require('convert-csv-to-json'); 
var inputFilePath1 = '../data/matches.csv'; 
var arrayOfMatches = csvToJson.fieldDelimiter(",").getJsonFromCsv(inputFilePath1);
var stat1Function = require("./stat1.js");
var output1 = stat1Function(arrayOfMatches);

var fs = require("fs");
var dataToWrite1 = JSON.stringify(output1);
var outputFilePath1 = '../output/numberOfMatchesPerYear.json'; 
fs.writeFile(outputFilePath1, dataToWrite1, (err) => {
  if (err) {
      console.log(err);
  }
  console.log("Successfully Written to File.");
});

// solving stat 2 
var stat2Function = require("./stat2.js");
var output2 = stat2Function(arrayOfMatches);
var dataToWrite2 = JSON.stringify(output2);
var outputFilePath2 = '../output/numberOfMatchesWonPerTeamPerYear.json'; 
fs.writeFile(outputFilePath2, dataToWrite2, (err) => {
  if (err) {
      console.log(err);
  }
  console.log("Successfully Written to File.");
});
//console.log(output2);

// solving stat 3
var inputFilePath2 = '../data/deliveries.csv'; 
var arrayOfDeliveries = csvToJson.fieldDelimiter(",").getJsonFromCsv(inputFilePath2);
var stat3Function = require("./stat3.js");
var output3 = stat3Function(arrayOfDeliveries, arrayOfMatches);
var dataToWrite3 = JSON.stringify(output3);
var outputFilePath3 = '../output/extraRunsConcededPerTeamIn2016.json'; 
fs.writeFile(outputFilePath3, dataToWrite3, (err) => {
  if (err) {
      console.log(err);
  }
  console.log("Successfully Written to File.");
});

// solving stat 4
var stat4Function = require("./stat4.js");
var output4 = stat4Function(arrayOfDeliveries, arrayOfMatches);
var dataToWrite4 = JSON.stringify(output4);
var outputFilePath4 = '../output/top10EconomicalBowlersIn2015.json'; 
fs.writeFile(outputFilePath4, dataToWrite4, (err) => {
  if (err) {
      console.log(err);
  }
  console.log("Successfully Written to File.");
});

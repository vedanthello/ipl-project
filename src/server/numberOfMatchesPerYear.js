let requestURL = "/numberOfMatchesPerYear.json";
let request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function () {
  let jsonObject = request.response;
  let years = [];
  let numberOfMatches = [];
  for (let year in jsonObject) {
    years.push(Number(year));
    numberOfMatches.push(jsonObject[year]);
  }
  let options = {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: years
    },
    yAxis: {
      title: {
        text: 'Number of matches'
      }
    },
    series: [
      {
        name: "number of matches",
        showInLegend: false,
        data: numberOfMatches
      }
    ]
  };
  Highcharts.chart('container', options);
};
let http = require("http");
let fs = require("fs");
let requestAndResponse = function(request, response) {
  console.log(request.url);  
  let filePath = "";
  let jsonFileNames = ["/numberOfMatchesPerYear.json", "/numberOfMatchesWonPerTeamPerYear.json",
                       "/extraRunsConcededPerTeamIn2016.json", "/top10EconomicalBowlersIn2015.json"];
  if (request.url === "/") {
    filePath = "./index.html";
    fs.readFile(filePath, (error, content) => {
      if (error) {
        console.log("Error in reading file");
        response.writeHead(404);
        response.end();  
      } else {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.end(content);  
        console.log("No error in reading file");
      } 
    });
  } else if (jsonFileNames.includes(request.url)) {
    filePath = "./../output" + request.url;
    fs.readFile(filePath, (error, content) => {
      if (error) {
        console.log("Error in reading file");
        response.writeHead(404);
        response.end();  
      } else {
        response.writeHead(200, {'Content-Type': 'text/json'});
        response.end(content);  
        console.log("No error in reading file");
      } 
    });
  } else {
    filePath = "." + request.url;
    fs.readFile(filePath, (error, content) => {
      if (error) {
        console.log("Error in reading file");
        response.writeHead(404);
        response.end();  
      } else {
        response.writeHead(200, {'Content-Type': 'text/json'});
        response.end(content);  
        console.log("No error in reading file");
      } 
    });
  }
};
http.createServer(requestAndResponse).listen(7000);
console.log("Hi");
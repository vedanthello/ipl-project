function stat3Function(deliveriesArray, matchesArray) {
    /**
     * Function parameters:
     * deliveriesArray - it is deliveries.csv parsed as array of objects, where each object describes a delivery.
     * matchesArray - it is matches.csv parsed as array of objects, where each object describes a match.
     * 
     * This function solves stat 3
     * This function returns the result of stat 3 as a JavaScript object.
     */
    // finding matches happened in 2016
    var matchIDs2016 = matchesArray.filter(match => match.season === "2016").map(match => match.id);
    var output = {};
    for (let delivery of deliveriesArray) {
      let {match_id, bowling_team, extra_runs} = delivery; 
      if (matchIDs2016.includes(match_id)) {
        if (!output.hasOwnProperty(bowling_team)) {
          output[bowling_team] = Number(extra_runs);
        } else {
          output[bowling_team] += Number(extra_runs);
        }
      } else {
        // ignore this delivery
      }
    }
    return output;
}
module.exports = stat3Function; 

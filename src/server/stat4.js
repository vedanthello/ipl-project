function stat4Function(deliveriesArray, matchesArray) {
    /**
     * Function parameters:
     * deliveriesArray - it is deliveries.csv parsed as array of objects, where each object describes a delivery.
     * matchesArray - it is matches.csv parsed as array of objects, where each object describes a match.
     * 
     * This function solves stat 4
     * This function returns the result of stat 4 as a JavaScript array.
     */
    // finding matches happened in 2015
    let matchIDs2015 = matchesArray.filter(match => match.season === "2015").map(match => match.id);
    let deliveriesIn2015 = deliveriesArray.filter(delivery => matchIDs2015.includes(delivery.match_id));
    let bowlers = deliveriesIn2015.reduce((bowlers, delivery) => {
      let {bowler, wide_runs, noball_runs, batsman_runs, match_id, over} = delivery;
      if (!bowlers.hasOwnProperty(bowler)) {
        bowlers[bowler] = {};
        bowlers[bowler].runsConceded = Number(wide_runs) + Number(noball_runs) + Number(batsman_runs);
        bowlers[bowler].match_idOver = [JSON.stringify([Number(match_id), Number(over)])];
      } else {
        bowlers[bowler].runsConceded += Number(wide_runs) + Number(noball_runs) + Number(batsman_runs);
        let match_idOverJSON = JSON.stringify([Number(match_id), Number(over)]);
        if (!bowlers[bowler].match_idOver.includes(match_idOverJSON)) {
          bowlers[bowler].match_idOver.push(match_idOverJSON);
        } else {
          // this delivery is of the same over
        }
      }
      return bowlers;
    }, {});
    let economyRates = [];
    for (let bowler in bowlers) {
      let bowlerObject = {};
      bowlerObject.Name = bowler;
      let numberOfOvers = bowlers[bowler].match_idOver.length;
      let economyRate = bowlers[bowler].runsConceded / numberOfOvers;
      bowlerObject["Economy Rate"] = economyRate;
      economyRates.push(bowlerObject);
    }
    // sorting the bowlers in the ascending order of their economy rates
    economyRates.sort((bowlerA, bowlerB) => bowlerA["Economy Rate"] - bowlerB["Economy Rate"]);
    
    return economyRates.slice(0, 10);
  }
module.exports = stat4Function; 

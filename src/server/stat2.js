function stat2Function (matchesArray) {
  /**
   * Function parameters:
   * matchesArray - it is matches.csv parsed as array of objects, where each object describes about a match.
   * 
   * This function solves stat 2
   * This function returns number of matches won per team per year as a JavaScript object.
   */
  var output = {};
  for (let i = 0; i <= (matchesArray.length - 1); i++) {
    var {winner, season} = matchesArray[i];
    if (!output.hasOwnProperty(winner)) {
      // winner not present in output
      output[winner] = {};
      output[winner][season] = 1;
    } else {
      // winner present in output
      if (!output[winner].hasOwnProperty(season)) {
        // winner's season not present in output
        output[winner][season] = 1;
      } else {
        // winner's season present in output
        output[winner][season]++;
      }  
    }
  }
  return output;
}
module.exports = stat2Function;
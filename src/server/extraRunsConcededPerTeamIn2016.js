let requestURL = "/extraRunsConcededPerTeamIn2016.json";
let request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function () {
  let jsonObject = request.response;
  let xValues = [];
  let yValues = [];
  for (let xValue in jsonObject) {
    xValues.push(xValue);
    yValues.push(jsonObject[xValue]);
  }
  let options = {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Extra runs conceded per team in 2016'
    },
    xAxis: {
      title: {
          text: 'Team',
      },
      categories: xValues,
    },
    yAxis: {
      title: {
        text: 'Extra runs conceded'
      }
    },
    series: [
      {
        name: "Extra runs conceded",
        showInLegend: false,
        data: yValues
      }
    ]
  };
  Highcharts.chart('container', options);
};
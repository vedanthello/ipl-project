function numberOfMatchesPerYear (matchesArray) {
    /**
     * Function parameters:
     * matchesArray - it is matches.csv parsed as array of objects, where each object describes about a match.
     * 
     * This function solves stat 1
     * This function returns number of matches per year as a JavaScript object.
     */
    var output = {};
    for (var i = 0; i <= (matchesArray.length - 1); i++) {
      if (output.hasOwnProperty(matchesArray[i].season)) {
        output[matchesArray[i].season]++;
      } else {
        output[matchesArray[i].season] = 1;
      }
    }
    return output;
  }
module.exports = numberOfMatchesPerYear;   
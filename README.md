# Description:

In this data assignment you will transform raw data of IPL to calculate the following stats:
1. Number of matches played per year for all the years in IPL.
2. Number of matches won of per team per year in IPL.
3. Extra runs conceded per team in 2016
4. Top 10 economical bowlers in 2015

Implement the 4 functions, one for each task.
Use the functions results to dump json files in the output folder

Directory structure:

- src/
  - server/
    - stat1.js
    - stat2.js
    - stat3.js
    - stat4.js
    - index.js
  - output/
    - numberOfMatchesPerYear.json
    - numberOfMatchesWonPerTeamPerYear.json
    - extraRunsConcededPerTeamIn2016.json
    - top10EconomicalBowlersIn2015.json
  - data/
    - matches.csv
    - deliveries.csv
- package.json
- package-lock.json
- .gitignore
- README.md
- node_modules/

# Bugs:

1. The package "convert-csv-to-json" parses a cell into more cells if that cell contains a ",".

# IPL Project 2 
## Task1:
HTTP Server to serve different static files at different endpoints. No external packages to be used.
## Task 2:
Serve one single html file. This should render in the browser at /.
## Task 3:
Use HighCharts to render a chart for each of the questions you had solved
### Checklist:
1. Must use the http routes you created for each of the solutions you had built
2. Appropriate status codes for routes
3. One html file for each function
4. Each html file must have atleast all the minimum needed HTML tags
5. One index.html file that links to the html file for each of the functions
6. Bonus: Frontend javascript in a separate file